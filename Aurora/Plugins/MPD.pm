# Copyright (c) 2014-2015 Jakob Nixdorf <flocke@shadowice.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

package Aurora::Plugins::MPD;

use strict;
use warnings;
use IO::Async::Stream;
use IO::Socket::INET;
use IO::Socket::UNIX;

use Aurora::Helpers;

sub new {
  my ( $class_name, $channel, $config ) = @_;
  my $self = {};
  bless ( $self, $class_name );
  $self->{_channel} = $channel;
  $self->{_config} = set_config ( $config, {
      label => 'mpd',
      format => '%{state}%{song_info}',
      format_songinfo => '%{Title} (%{Artist})',
      state_play => '',
      state_pause => 'Paused: ',
      state_stop => '',
      type => 'net',
      host => 'localhost',
      port => 6600,
      socket => "$ENV{HOME}/.config/mpd/socket"
  } );
  $self -> start();
  $self->{_created} = 1;
  return ( $self );
}

sub start {
  my ( $self ) = @_;

  if ($self->{_config}->{type} eq 'net') {
    $self->{_socket} = IO::Socket::INET -> new (
      PeerHost => $self->{_config}->{host},
      PeerPort => $self->{_config}->{port},
      Proto => 'tcp'
    );
  } elsif ($self->{_config}->{type} eq 'local') {
    $self->{_socket} = IO::Socket::UNIX -> new (
      Type => SOCK_STREAM(),
      Peer => $self->{_config}->{socket}
    );
  }

  $self->{_handle} = IO::Async::Stream -> new (
    handle => $self->{_socket},
    on_read => sub {
      my ( $mystream, $buffer, $eof ) = @_;

      my $text = $$buffer;
      $$buffer = "";

      if ( $text =~ m/^changed:\s+?player/ or $text =~ m/^OK\s+MPD/ ) {
        $self -> update_mpd_info ( $mystream );
      } else {
        $mystream -> write ( "idle\n" );
      }

      return 1;
    }
  );
}

sub update_mpd_info {
  my ( $self, $stream ) = @_;

  $stream -> push_on_read ( sub {
      my ( $mystream, $buffer, $eof ) = @_;

      my $cache = {};

      foreach my $line ( split ( /\n/, $$buffer ) ) {
        if ( $line =~ m/^(.+?):\s+(.+?)$/ ) {
          $cache->{$1} = $2;
        }
      }

      $$buffer = "";

      $self -> send_update ( $cache );

      return undef;
    }
  );

  $stream -> write ( "status\ncurrentsong\n" );
}

sub send_update {
  my ( $self, $info ) = @_;

  my $hash = {};

  if ( $info->{state} eq "play" or $info->{state} eq "pause" ) {
    my $state_name = "state_" . $info->{state};
    $hash->{song_info} = format_string_from_hash ( $self->{_config}->{format_songinfo}, $info );
    $hash->{state} = $self->{_config}->{$state_name};
  } else {
    $hash->{song_info} = "";
    $hash->{state} = $self->{_config}->{state_stop};
  }

  $self->{_channel} -> send ( { $self->{_config}->{label} => format_string_from_hash ( $self->{_config}->{format}, $hash ) } );
}

sub exit {
  my ( $self ) = @_;
  if ( $self->{_handle} ) { $self->{_handle} -> close() };
  if ( $self->{_stream} ) { $self->{_stream} -> close() };
};

1;
