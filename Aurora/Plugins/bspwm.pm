# Copyright (c) 2014-2015 Jakob Nixdorf <flocke@shadowice.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

package Aurora::Plugins::bspwm;

use strict;
use warnings;
use IO::Async::Process;

use Aurora::Helpers;

sub new {
  my ( $class_name, $channel, $config ) = @_;
  my $self = {};
  bless ( $self, $class_name );
  $self->{_channel} = $channel;
  $self->{_config} = set_config ( $config, {
      label => 'bspwm',
      format => '%{tags}',
      tag_format => '%{start_fg}%{start_bg}  %{tag}  %{end_fg}%{end_bg}',
      tag_separator => '',
      focused_fg => '',
      focused_bg => '',
      used_fg => '',
      used_bg => '',
      urgent_fg => '',
      urgent_bg => ''
  } );
  $self -> start();
  $self->{_created} = 1;
  return ( $self );
}

sub start {
  my ( $self ) = @_;

  $self->{_handle} = IO::Async::Process -> new (
    command => [ 'bspc', 'control', '--subscribe' ],
    stdout => {
      on_read => sub {
        my ( $stream, $buffer ) = @_;
        while ( $$buffer =~ s/^(.*)\n// ) {
          $self -> update($1);
        };
      }
    },
    on_finish => sub {}
  );
}

sub update {
  my ( $self, $text ) = @_;

  $text =~ s/^\s+//g;
  $text =~ s/\s+$//g;
  my @tags = split ( ':', $text );

  for my $tag ( @tags ) {
    $tag =~ m/^(.)(.+?)$/;
    $tag = $self -> format_tag ( $self->{_config}->{tag_format}, $1, $2 );
  }

  my $hash = { tags => join ( $self->{_config}->{tag_separator}, @tags ) };

  $self->{_channel} -> send ( { $self->{_config}->{label} => format_string_from_hash ( $self->{_config}->{format}, $hash ) } );
}

sub format_tag {
  my ( $self, $format, $type, $output ) = @_;

  my $work_string = $format;

  my $hash = {
    tag => $output,
    start_fg => "",
    start_bg => "",
    end_fg => "^fg()",
    end_bg => "^bg()"
  };

  if ( $type eq "O" or $type eq "F" or $type eq "U" ) {
    if ( $self->{_config}->{focused_fg} ) { $hash->{start_fg} = "^fg(" . $self->{_config}->{focused_fg} . ")" };
    if ( $self->{_config}->{focused_fg} ) { $hash->{start_bg} = "^bg(" . $self->{_config}->{focused_bg} . ")" };
    return format_string_from_hash ( $work_string, $hash );
  } elsif ( $type eq "o" ) {
    if ( $self->{_config}->{used_fg} ) { $hash->{start_fg} = "^fg(" . $self->{_config}->{used_fg} . ")" };
    if ( $self->{_config}->{used_bg} ) { $hash->{start_bg} = "^bg(" . $self->{_config}->{used_bg} . ")" };
    return format_string_from_hash ( $work_string, $hash );
  } elsif ( $type eq "u" ) {
    if ( $self->{_config}->{urgent_fg} ) { $hash->{start_fg} = "^fg(" . $self->{_config}->{urgent_fg} . ")" };
    if ( $self->{_config}->{urgent_bg} ) { $hash->{start_bg} = "^bg(" . $self->{_config}->{urgent_bg} . ")" };
    return format_string_from_hash ( $work_string, $hash );
  } elsif ( $type eq "f" ) {
    return format_string_from_hash ( $work_string, $hash );
  }
}

sub exit {
  my ( $self ) = @_;
  if ( $self->{_handle} ) { $self->{_handle} -> kill ( 'INT' ) };
}

1;
