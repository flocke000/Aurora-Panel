# Copyright (c) 2014-2015 Jakob Nixdorf <flocke@shadowice.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

package Aurora::Plugins::Aria2RPC;

use strict;
use warnings;
use IO::Async::Timer::Periodic;

use RPC::XML;
use RPC::XML::Client;

use Aurora::Helpers;

sub new {
  my ( $class_name, $channel, $config ) = @_;
  my $self = {};
  bless ( $self, $class_name );
  $self->{_channel} = $channel;
  $self->{_config} = set_config ( $config, {
      timeout => 2,
      label => 'aria2',
      host => 'localhost',
      port => 6800,
      format => '%{num}%{info}',
      info_format => ' | %{percentage}% | %{time}'
  } );
  $self -> start();
  $self->{_created} = 1;
  return ( $self );
}

sub start {
  my ( $self ) = @_;

  my $cli = RPC::XML::Client -> new ( "http://" . $self->{_config}->{host} . ":" . $self->{_config}->{port} . "/rpc" );

  $self->{_handle} = IO::Async::Timer::Periodic -> new (
    first_interval => 0,
    interval => $self->{_config}->{timeout},
    on_tick => sub {
      my $resp = $cli -> send_request ( 'aria2.tellActive' );

      my $hash = {};

      if ( ref $resp ) {
        my @list = @{$resp -> value()};
        my ($count, $total, $down) = (0, 0, 0);
        my @times;

        for my $dl (@list) {
          if ($dl->{status} eq "active") {
            if ( $dl->{downloadSpeed} > 0 ) {
              $count++;
              $total += $dl->{totalLength};
              $down += $dl->{completedLength};
              push(@times, ($dl->{totalLength} - $dl->{completedLength})/$dl->{downloadSpeed});
            }
          }
        }

        $hash->{num} = $count;
        if ( $count > 0 && $total > 0 ) {
          $hash->{percentage} = sprintf ( "%.0f", ( $down * 100 ) / $total );
          $hash->{time} = sec_to_timestr ( $times[0] );
          $hash->{info} = format_string_from_hash ( $self->{_config}->{info_format}, $hash );
        } else {
          $hash->{info} = "";
        }
      } else {
        $hash->{num} = "Not Running!";
        $hash->{info} = "";
      }
      $self->{_channel} -> send ( { $self->{_config}->{label} => format_string_from_hash ( $self->{_config}->{format}, $hash ) } );
    }
  );

  $self->{_handle} -> start();
}

sub exit { };

1;
