# Copyright (c) 2014-2015 Jakob Nixdorf <flocke@shadowice.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

package Aurora::Plugins::PulseAudio;

use strict;
use warnings;
use IO::Async::Timer::Periodic;

use Aurora::Helpers;

sub new {
  my ( $class_name, $channel, $config ) = @_;
  my $self = {};
  bless ( $self, $class_name );
  $self->{_channel} = $channel;
  $self->{_config} = set_config ( $config, {
      timeout => 2,
      label => 'pulseaudio',
      format => '%{default_volume}',
      volume_format => '%{volume}%',
      volume_format_muted => '%{volume}% (Muted)'
  } );
  $self -> start();
  $self->{_created} = 1;
  return ( $self );
}

sub start {
  my ( $self ) = @_;

  $self->{_handle} = IO::Async::Timer::Periodic -> new (
    first_interval => 0,
    interval => $self->{_config}->{timeout},
    on_tick => sub {
      my $default_sink = $self -> match_pacmd ( 'stat', '^Default sink name: (.+?)\n' );
      my @sink_names = $self -> match_pacmd ( 'list-sinks', '^\s+?name: <(.+?)>\n' );
      my @sink_volumes = $self -> match_pacmd ( 'list-sinks', '^\s+?volume:\s+?front-left:\s+?\d+?\s+/\s+(\d+)%' );
      my @sink_mute = $self -> match_pacmd ( 'list-sinks', '^\s+?muted:\s+?(no|yes)\n' );

      my $hash = {};
      for ( my $i = 0; $i < scalar ( @sink_names ); $i++) {
        my $name = $sink_names[$i];
        my $default = 0;

        if ( $name eq $default_sink ) { $default = 1 };

        if ( $self->{_config}->{sink_names} && $self->{_config}->{sink_names}->{$name} ) {
          $name = $self->{_config}->{sink_names}->{$name};
        }

        $name = "volume_" . $name;

        if ( $sink_mute[$i] eq "yes" ) {
          $hash->{$name} = format_string_from_hash ( $self->{_config}->{volume_format_muted}, { volume => $sink_volumes[$i] } );
        } else {
          $hash->{$name} = format_string_from_hash ( $self->{_config}->{volume_format}, { volume => $sink_volumes[$i] } );
        }

        if ( $default ) { $hash->{default_volume} = $hash->{$name} };
      }

      if ( $self->{_config}->{sink_names} && $self->{_config}->{sink_names}->{$default_sink} ) {
        $hash->{default_sink} = $self->{_config}->{sink_names}->{$default_sink};
      }

      $self->{_channel} -> send ( { $self->{_config}->{label} => format_string_from_hash ( $self->{_config}->{format}, $hash ) } );
    }
  );

  $self->{_handle} -> start();
}

sub match_pacmd {
  my ( $self, $cmd, $match ) = @_;

  my @return;
  open ( PACMD, "pacmd $cmd |" );
  foreach my $line ( <PACMD> ) {
    if ( $line =~ m/$match/ ) {
      push ( @return, $1 );
    }
  }
  close ( PACMD );

  if ( scalar ( @return ) == 1 ) {
    return ( $return[0] );
  } else {
    return ( @return );
  }
}

sub exit { };

1;
