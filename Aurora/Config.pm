# Copyright (c) 2014-2015 Jakob Nixdorf <flocke@shadowice.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

package Aurora::Config;

use Aurora::Helpers;

use XML::TreePP;

sub read {
  my ( $configfile ) = @_;

  my $tpp = XML::TreePP -> new();

  if ( $configfile && ! -f $configfile ) { die "ERROR: Could not open user-defined config file (" . $configfile . ")\n" };

  if ( ! $configfile ) { $configfile = "$ENV{HOME}/.config/aurora-panel/config.xml" };
  if ( ! -f $configfile ) { $configfile = "config.xml" };
  if ( ! -f $configfile ) {
    die ( "ERROR: Configuration file not found.\n" );
  } else {
    my $config = $tpp -> parsefile ( $configfile );
    my $default_config = {
      debug => 0,
      pidfile => "$ENV{HOME}/.config/aurora-panel/running.pid",
      logfile => "$ENV{HOME}/.config/aurora-panel/aurora.log"
    };
    return ( set_config ( $config, $default_config ) );
  }
};

1;

