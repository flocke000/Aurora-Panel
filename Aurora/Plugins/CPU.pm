# Copyright (c) 2014-2015 Jakob Nixdorf <flocke@shadowice.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

package Aurora::Plugins::CPU;

use strict;
use warnings;
use IO::Async::Timer::Periodic;

use Aurora::Helpers;

sub new {
  my ( $class_name, $channel, $config ) = @_;
  my $self = {};
  bless ( $self, $class_name );
  $self->{_channel} = $channel;
  $self->{_config} = set_config ( $config, {
      timeout => 1,
      label => 'cpu',
  } );
  $self -> start();
  $self->{_created} = 1;
  return ( $self );
}

sub start {
  my ( $self ) = @_;

  my @loads;

  $self->{_handle} = IO::Async::Timer::Periodic -> new (
    first_interval => 0,
    interval => $self->{_config}->{timeout},
    on_tick => sub {
      open ( STAT, "</proc/stat" );
      for my $line ( <STAT> ) {
        if ( $line =~ m/^cpu\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s/ ) {
          $loads[0] = $1;
          $loads[1] = $2;
          $loads[2] = $3;
          $loads[3] = $4;
        }
      }
      close ( STAT );

      if ( scalar ( @loads ) > 4 ) {
        my $used = $loads[0] + $loads[1] + $loads[2] - $loads[4] - $loads[5] - $loads[6];
        my $total = $used + $loads[3] - $loads[7];
        if ( $total > 0 ) { 
          $self->{_channel} -> send ( { $self->{_config}->{label} => sprintf ( "%.0f", ( $used / $total ) * 100 ) } );
        }
     }

     for my $i (0..3) { $loads[$i+4] = $loads[$i] };
    }
  );

  $self->{_handle} -> start();
}

sub exit { };

1;
