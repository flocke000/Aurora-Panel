# Copyright (c) 2014-2015 Jakob Nixdorf <flocke@shadowice.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

package Aurora::Helpers;

use Exporter 'import';
@EXPORT = qw(calculate_right_offset calculate_center_offset format_string_from_hash sec_to_timestr set_config);

sub calculate_right_offset {
  my ( $string, $screen_width, $font_width ) = @_;

  my $clean_string = $string;
  $clean_string =~ s/\^[^\(]*\([^\)]*\)//g;

  return ( $screen_width - ( length ( $clean_string ) * $font_width ) );
}

sub calculate_center_offset {
  return ( calculate_right_offset ( @_ ) / 2 );
}

sub format_string_from_hash {
  my ( $string, $hash ) = @_;

  my $work_string = $string;

  foreach my $key ( keys ( %$hash ) ) {
    $work_string =~ s/\%\{$key\}/$hash->{$key}/g;
  }

  return ( $work_string );
}

sub sec_to_timestr {
  my ( $time ) = @_;

  my $sec = sprintf("%02u", $time%60);
  my $min = sprintf("%02u", ($time/60)%60);
  my $hour = sprintf("%02u", ($time/(60*60))%24);

  return ( "$hour:$min:$sec" );
}

sub set_config {
  my ( $config, $default ) = @_;

  foreach my $key ( %$default ) {
    if ( ! $config->{$key} ) { $config->{$key} = $default->{$key} };
  }

  return ( $config );
}

1;
