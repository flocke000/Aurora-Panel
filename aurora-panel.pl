#! /usr/bin/perl
#
# Copyright (c) 2014-2015 Jakob Nixdorf <flocke@shadowice.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

use strict;
use warnings;

my $VERSION = "0.1-dev";

use File::Path qw(make_path);
use Getopt::Long;
use IO::Async::Loop;
use IO::Async::Channel;
use IO::Async::Routine;
use IO::Async::Signal;
use Module::Load qw(load);
use Module::Load::Conditional qw(can_load);
use POSIX qw(setsid);

$IO::Async::Loop::LOOP = "Select";

use Aurora::Config;
use Aurora::Helpers;

my $opt_configfile;
my $opt_pidfile;
my $opt_logfile;
my $opt_daemonize;
my $opt_help;

GetOptions (
  "config|c=s" => \$opt_configfile,
  "pidfile|p=s" => \$opt_pidfile,
  "logfile|l=s" => \$opt_logfile,
  "daemon|d" => \$opt_daemonize,
  "help|h" => \$opt_help
) or die "ERROR: Faild to parse all command line arguments.\n";

my $config = Aurora::Config::read( $opt_configfile );

if ( $opt_help ) {
  print_help();
  exit 0;
}

my @loaded_plugins;
my $cache = {};

if ( $opt_daemonize ) { daemonize() };
write_pidfile();

my $main_loop = IO::Async::Loop -> new();

my $input_channel = IO::Async::Channel -> new ();
my $main_worker = IO::Async::Routine -> new (
  channels_in => [ $input_channel ],
  code => \&main_worker,
  on_finish => \&exit_clean
);

my $signal_term = IO::Async::Signal -> new ( name => 'TERM', on_receipt => \&exit_clean );
my $signal_int = IO::Async::Signal -> new ( name => 'INT', on_receipt => \&exit_clean );

$main_loop -> add ( $signal_term );
$main_loop -> add ( $signal_int );
$main_loop -> add ( $main_worker );
if ( $config->{debug} ) { print "DEBUG: Added signal handler and input channel.\n" };

if ( ref ( $config->{plugin} ) eq 'ARRAY' ) {
  foreach my $plugin ( @{$config->{plugin}} ) { load_plugin ( $plugin ) };
} else {
  load_plugin ( $config->{plugin} );
}

if ( $config->{debug} ) { print "DEBUG: Starting the main loop.\n" };
my $running = 1;
$main_loop -> run();

exit_clean();

###############################################################################
### Main function

sub main_worker {
  my @panels;

  if ( ref ( $config->{panel} ) eq 'ARRAY' ) {
    @panels = @{$config->{panel}};
  } else {
    $panels[0] = $config->{panel};
  }

  foreach my $panel ( @panels ) {
    open ( $panel->{_handle}, "| dzen2 -w " . $panel->{width} . " -h " . $panel->{height} . " -x " . $panel->{x_offset} . " -y " . $panel->{y_offset} . " -fn '" . $panel->{font} . "' -fg '" . $panel->{fg} . "' -bg '" . $panel->{bg} . "'" );
    autoflush {$panel->{_handle}} 1;
  }
  if ( $config->{debug} ) { print "DEBUG: Loaded all panels.\n" };

  while ( my $inref = $input_channel -> recv() ) {
    for my $key ( keys ( %$inref ) ) {
      if ( $key eq "quit_panel" ) {
        if ( $inref->{$key} != 0 ) {
          return;
        }
      }

      $cache->{$key} = $inref->{$key};
    }

    foreach my $panel ( @panels ) {
      my $work_left_string = format_string_from_hash ( $panel->{left_string}, $cache );
      my $work_center_string = format_string_from_hash ( $panel->{center_string}, $cache );
      my $work_right_string = format_string_from_hash ( $panel->{right_string}, $cache );

      my $right_offset = calculate_right_offset ( $work_right_string, $panel->{width}, $panel->{font_width} );
      my $center_offset = calculate_center_offset ( $work_center_string, $panel->{width}, $panel->{font_width} );

      print {$panel->{_handle}} $work_left_string . "^pa(" . $center_offset . ")" . $work_center_string . "^pa(" . $right_offset . ")" . $work_right_string . "\n";
    }
  }
}

###############################################################################
### Helper functions

sub load_plugin {
  my ( $plugin ) = @_;

  my $module = "Aurora::Plugins::" . $plugin->{-type};

  if ( can_load ( module => { "$module" => undef } ) ) {
    load "$module";
    $plugin->{_object} = "$module" -> new( $input_channel, $plugin );
    if ( $plugin->{_object} && $plugin->{_object}->{_handle} ) {
      $plugin->{_module_name} = $module;
      push ( @loaded_plugins, $plugin );
      $main_loop -> add ( $plugin->{_object}->{_handle} );
      if ( $config->{debug} ) { print "DEBUG: Loaded plugin '" . $plugin->{-type} . "' ($module).\n" };
    } else {
      warn "ERROR: Failed to create object for plugin '$plugin->{-type}' ($module).\n";
    }
  } else {
    warn "ERROR: Could not find plugin '$plugin->{-type}' ($module).\n";
  }
}

sub print_help {
  print "Aurora Panel (v$VERSION)\n";
  print "\n";
  print "Usage: aurora-panel.pl [options]\n";
  print "\n";
  print "Without arguments this spawns the Aurora Panel with the options given in the main configuration file.\n";
  print "The configuration file must be located either in the current working directory or in ~/.config/aurora-panel.\n";
  print "\n";
  print "Options:\n";
  print "  -h, --help\t\t\tPrint this help message and exit.\n";
  print "  -d, --daemon\t\t\tFork into a background process after start.\n";
  print "  -c, --config=FILE\t\tUse a different configuration file.\n";
  print "  -l, --logfile=FILE\t\tUse a different logfile (default: " . $config->{logfile} . ").\n";
  print "  -p, --pidfile=FILE\t\tUsa a different pidfile (default: " . $config->{pidfile} . ")\n";
  print "\n";
}

sub create_filedir {
  my ( $file ) = @_;

  $file =~ m/^(.+)\/(.+?)$/;
  my $dir = $1;

  if ( ! -d $dir ) { make_path ( $dir ) };
}

sub daemonize {
  if ( ! $opt_logfile ) { $opt_logfile = $config->{logfile} };
  create_filedir ( $opt_logfile );

  open ( STDIN, "</dev/null" ) or die "ERROR: Failed to rebind STDIN to /dev/null: $!\n";
  open ( STDOUT, ">" . $opt_logfile ) or die "ERROR: Failed to rebind STDOUT to " . $opt_logfile . ": $!\n";

  defined ( my $pid = fork() ) or die "ERROR: Failed to fork!\n";
  exit if ( $pid );
  ( setsid() != -1 ) or die "ERROR: Failed to create a new session: $!\n";

  open ( STDERR, ">&STDOUT" ) or die "ERROR: Failed to redirect STDERR to STDOUT: $!\n";
}

sub write_pidfile {
  if ( ! $opt_pidfile ) { $opt_pidfile = $config->{pidfile} };

  create_filedir ( $opt_pidfile );

  open ( PIDFILE, ">" . $opt_pidfile );
  print PIDFILE $$;
  close ( PIDFILE );
}

sub exit_clean {
  if ( $running ) {
    if ( $config->{debug} ) { print "DEBUG: Exiting clean.\n" };

    $main_loop -> stop();
    if ( $config->{debug} ) { print "DEBUG: Stopped the main loop.\n" };

    $input_channel -> close();
    if ( $config->{debug} ) { print "DEBUG: Closed the input channel.\n" };

    foreach my $plugin ( @loaded_plugins ) {
      if ( $config->{debug} ) { print "DEBUG: Unloaded plugin '" . $plugin->{-type} . "' (" . $plugin->{_module_name} . ").\n" };
      $plugin->{_object} -> exit();
    }
  
    if ( ref ( $config->{panel} ) eq 'ARRAY' ) {
      for my $panel ( @{$config->{panel}} ) {
        if ( $panel->{_handle} ) { close ( $panel->{_handle} ) };
      }
    } else {
      if ( $config->{panel}->{_handle} ) { close ( $config->{panel}->{_handle} ) };
    }
    if ( $config->{debug} ) { print "DEBUG: Stopped all panels.\n" };

    if ( -f $opt_pidfile ) {
      if ( $config->{debug} ) { print "DEBUG: Removing pidfile.\n" };
      unlink ( $opt_pidfile );
    }

    $running = 0;
  }
}

