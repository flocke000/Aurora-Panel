# Copyright (c) 2014-2015 Jakob Nixdorf <flocke@shadowice.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

package Aurora::Plugins::Network;

use strict;
use warnings;
use IO::Async::Timer::Periodic;

use Aurora::Helpers;

sub new {
  my ( $class_name, $channel, $config ) = @_;
  my $self = {};
  bless ( $self, $class_name );
  $self->{_channel} = $channel;
  $self->{_config} = set_config ( $config, {
      timeout => 1,
      device => 'eth0',
      label => 'network',
      format => '%{net_down} kb| %{net_up} kb'
  } );
  $self -> start();
  $self->{_created} = 1;
  return ( $self );
}

sub start {
  my ( $self ) = @_;

  my ( $rx_prev, $tx_prev );

  $self->{_handle} = IO::Async::Timer::Periodic -> new (
    first_interval => 0,
    interval => $self->{_config}->{timeout},
    on_tick => sub {
      open ( RX, "</sys/class/net/" . $self->{_config}->{device} . "/statistics/rx_bytes" );
      open ( TX, "</sys/class/net/" . $self->{_config}->{device} . "/statistics/tx_bytes" );
      my $rx_cur = <RX>;
      my $tx_cur = <TX>;
      close ( RX );
      close ( TX );

      my $hash = {
        net_down => 0,
        net_up => 0
      };

      if ( $rx_prev ) { $hash->{net_down} = sprintf ( "%.0f", ( $rx_cur - $rx_prev ) / $self->{_config}->{timeout} / 1024 ) };
      if ( $tx_prev ) { $hash->{net_up} = sprintf ( "%.0f", ( $tx_cur - $tx_prev ) / $self->{_config}->{timeout} / 1024 ) };

      $rx_prev = $rx_cur;
      $tx_prev = $tx_cur;

      $self->{_channel} -> send ( { $self->{_config}->{label} => format_string_from_hash ( $self->{_config}->{format}, $hash ) } );
    }
  );

  $self->{_handle} -> start();
}

sub exit { };

1;
