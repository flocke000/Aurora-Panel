# Aurora Panel

## Requirements

### Main application

* [Perl](https://www.perl.org/) with:
  - [IO::Async](http://search.cpan.org/dist/IO-Async/)
  - Module::Load::Conditional
  - [XML::TreePP](http://search.cpan.org/dist/XML-TreePP/)

### Plugins

* **Aria2RPC**:
  - [RPC::XML::Client](http://search.cpan.org/dist/RPC-XML/)
* **IMAP**:
  - [Net::IMAP::Client](http://search.cpan.org/dist/Net-IMAP-Client/)
* **PulseAudio**:
  - *pacmd* in your **$PATH**

## Configuration

The main configuration happens in the file config.xml.
It should be located in ~/.config/aurora-panel or the
folder in which the application is started.
Look at the example in the application main directory.

Written by Jakob Nixdorf <flocke@shadowice.org>.
