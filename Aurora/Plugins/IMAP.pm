# Copyright (c) 2014-2015 Jakob Nixdorf <flocke@shadowice.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

package Aurora::Plugins::IMAP;

use strict;
use warnings;
use IO::Async::Timer::Periodic;

use Module::Load qw(load);
use Module::Load::Conditional qw(can_load);
use Net::IMAP::Client;

use Aurora::Helpers;

sub new {
  my ( $class_name, $channel, $config ) = @_;
  my $self = {};
  bless ( $self, $class_name );
  $self->{_channel} = $channel;
  $self->{_config} = set_config ( $config, {
      timeout => 600,
      label_prefix => 'imap_',
      notify => 0,
      notify_timeout => 10,
  } );
  $self -> start();
  $self->{_created} = 1;
  return ( $self );
}

sub start {
  my ( $self ) = @_;

  $self->{_handle} = IO::Async::Timer::Periodic -> new (
    first_interval => 0,
    interval => $self->{_config}->{timeout},
    on_tick => sub {
      my $return_hash = {};

      if ( $self->{_config}->{notify} && can_load ( module => { 'Desktop::Notify' => undef } ) ) {
        load 'Desktop::Notify';
        $self->{_notify} = 'Desktop::Notify' -> new();
      }

      if ( ref ( $self->{_config}->{account} ) eq 'ARRAY' ) {
        for my $acc ( @{$self->{_config}->{account}} ) {
          my $name = $self->{_config}->{label_prefix} . $acc->{-name};
          $return_hash->{$name} = $self -> fetch_mail ( $acc );
        }
      } else {
        my $name = $self->{_config}->{label_prefix} . $self->{_config}->{account}->{-name};
        $return_hash->{$name} = $self -> fetch_mail ( $self->{_config}->{account} );
      }

      $self->{_channel} -> send ( $return_hash );
    }
  );

  $self->{_handle} -> start();
}

sub send_notification {
  my ( $self, $title, $body ) = @_;

  foreach my $key ( %{$self->{_escape}} ) {
    $title =~ s/$key/$self->{_escape}->{$key}/g;
    $body =~ s/$key/$self->{_escape}->{$key}/g;
  }

  my $notification = $self->{_notify} -> create (
    summary => $title,
    body => $body,
    timeout => ( $self->{_config}->{notify_timeout} * 1000 )
  );

  $notification -> show();
}

sub fetch_mail {
 my ( $self, $acc ) = @_;

 my $imap_config = {};
 my $missing = 0;

 foreach my $key ( "server", "user", "pass" ) {
   if ( $acc->{$key} ) {
     $imap_config->{$key} = $acc->{$key};
   } else {
     $missing = 1;
   }
 }

 foreach my $key ( "ssl", "port" ) {
   if ( $acc->{$key} ) {
     $imap_config->{$key} = $acc->{$key};
   }
 }

 return "CONFIG ERROR" if ( $missing );

 my $mailbox = "INBOX";
 if ( $acc->{mailbox} ) {
   $mailbox = $acc->{mailbox};
 }

 my $client = Net::IMAP::Client -> new ( %$imap_config );
 $client -> login() or return "LOGIN ERROR";

 $client -> select ( $mailbox );
 my $unseen = $client -> search ( 'UNSEEN' );
 my $num = scalar ( @$unseen );

 if ( $self->{_notify} ) {
   my $new_msgs = $client -> search ( 'NEW' );
   my $sums = $client -> get_summaries ( $new_msgs );

   foreach my $msg ( @$sums ) {
     $self -> send_notification (
       "New mail in mailbox '" . $acc->{-name} . "'",
       "From: " . join ( ", ", @{$msg -> from()}) . "\n" . "Subject: " . $msg -> subject()
     );
   }
 }

 $client -> logout();

 return ( $num );
}

sub exit { };

1;
